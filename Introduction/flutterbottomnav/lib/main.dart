import 'package:flutter/material.dart';
import 'package:flutterbottomnav/ContactPage.dart';
import 'package:flutterbottomnav/HomePage.dart';
import 'package:flutterbottomnav/SettingsPage.dart';

void main() => runApp(
    BottomNav()
  );

class BottomNav extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: MyBottomNavigationBar(),
    );
  }
}

class MyBottomNavigationBar extends StatefulWidget {
  @override
  _MyBottomNavigationBarState createState() => _MyBottomNavigationBarState();
}

class _MyBottomNavigationBarState extends State<MyBottomNavigationBar> {
  int _currentIndex = 0;
  final List<Widget> _children = [
    Homepage(),
    SettingsPage(),
    ContactPage(),
  ];

  void onTappedBar(int index){
    setState(() {
      _currentIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: _children[_currentIndex],

      bottomNavigationBar: BottomNavigationBar(
        onTap: onTappedBar,
        currentIndex: _currentIndex,
        items: [
          BottomNavigationBarItem(
            icon: new Icon(Icons.home),
            title: new Text("Home"),
          ),

          BottomNavigationBarItem(
            icon: new Icon(Icons.settings),
            title: new Text("Settings"),
          ),

          BottomNavigationBarItem(
            icon: new Icon(Icons.contacts),
            title: new Text("Contact"),
          ),

        ]
      ),
    );
  }
}