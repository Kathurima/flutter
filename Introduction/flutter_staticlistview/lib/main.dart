import 'package:flutter/material.dart';

void main() => runApp(
  new MaterialApp(
    debugShowCheckedModeBanner: false,
      home: MyApp(),
  )
  );

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: Text("Static ListView"),
      ),

      body: MyStaticListView(),
    );
  }
}

class MyStaticListView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new ListView(
      children: ListTile.divideTiles(
        context: context,
        tiles: [
          ListTile(
            leading: Icon(Icons.directions_car),
            title: new Text("Car"),
            subtitle: new Text("This car is amazing"),
            onTap: (){
              print("Car");
            },
          ),

          ListTile(
            leading: Icon(Icons.bluetooth_searching),
            title: new Text("Bluetooth"),
            subtitle: new Text("Bluetooth is on"),
            onTap: (){
              print("Bluetooth");
            },
          ),

          ListTile(
            leading: Icon(Icons.wifi),
            title: new Text("WiFi"),
            subtitle: new Text("WiFi available"),
            onTap: (){
              print("WiFi");
            },
          ),

          ListTile(
            leading: Icon(Icons.collections),
            title: new Text("Collections"),
            subtitle: new Text("Cool collection"),
            onTap: (){
              print("Collections");
            },
          ),

          ListTile(
            leading: Icon(Icons.computer),
            title: new Text("Computer"),
            subtitle: new Text("Computers are available"),
            onTap: (){
              print("Computers");
            },
          )
        ],
      ).toList(),
    );
  }
}


