import 'package:flutter/material.dart';

void main(){
  runApp(
    MyApp()
  );
}

class MyApp extends StatelessWidget{
  Widget build(BuildContext context){
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: SearchAppBar(),
    );
  }
}

class SearchAppBar extends StatefulWidget {
  @override
  _SearchAppBarState createState() => _SearchAppBarState();
}

class _SearchAppBarState extends State<SearchAppBar> {
  Widget appBarTitle = new Text("Home");
  Icon actionIcon = new Icon(Icons.search);

  @override
  Widget build(BuildContext context) {
    //Creating app bar.
    //Since appbar is belongs to the scaffold class, we must create an instance
    // of the Scaffold
    return new Scaffold(
      appBar: new AppBar(
        title: appBarTitle,
        actions: <Widget>[
          new IconButton(
              icon: actionIcon,
              onPressed: (){
                setState(() {
                  if(this.actionIcon.icon == Icons.search){
                    //Closing the search icon and replacing the appbar title with textfield
                    this.actionIcon = new Icon(Icons.close);

                    this.appBarTitle = new TextField(
                      style: new TextStyle(
                        color: Colors.white
                      ),
                      decoration: new InputDecoration(
                        prefixIcon: new Icon(Icons.search, color: Colors.white),
                        hintText: "Search...",
                        hintStyle: new TextStyle(color: Colors.white),
                      ),
                    );
                  }else{
                    this.actionIcon = new Icon(Icons.search);
                    this.appBarTitle = new Text("Home");
                  }
                });
              }
          )
        ],
      ),
    );
  }
}
