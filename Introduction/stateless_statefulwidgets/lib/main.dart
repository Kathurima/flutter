import 'package:flutter/material.dart';

void main(){
  runApp(
    //App for Stateless widget
      //StatelessApp()
    //App for stateful widget
    StatefulApp()
  );
}

////Beginning of stateless implementation
//class StatelessApp extends StatelessWidget {
//  @override
//  Widget build(BuildContext context) {
//    return Container(
//      color: Colors.green,
//
//      child: Center(
//        child: Text(
//          "Stateless Widget",
//          textDirection: TextDirection.ltr,
//        ),
//      ),
//    );
//  }
//}

//Beginning of stateful implementation
class StatefulApp extends StatefulWidget {
  @override
  _StatefulAppState createState() => _StatefulAppState();
}

class _StatefulAppState extends State<StatefulApp> {
  @override
  Widget build(BuildContext context) {
    return Container();
  }
}
