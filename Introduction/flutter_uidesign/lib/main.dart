import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {

    Color color = Theme.of(context).primaryColor;

    Widget textSection = Container(
      padding: const EdgeInsets.all(32.0),
      child:Text(
        "Silicon Valley, in the southern San Francisco Bay Area of California,"
        " is home to many start-up and global technology companies. Apple, "
        "Facebook and Google are among the most prominent. It’s also the site "
        "of technology-focused institutions centered around Palo Alto's Stanford "
        "University. The Computer History Museum and NASA’s Ames Research Center "
        "are in Mountain View. The Tech Museum of Innovation is in San Jose.",
        softWrap: true,
        textAlign: TextAlign.justify,
      ),
    );

    Widget buttonSection = Container(

      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          _buildButtonColumn(color, Icons.call, "CALL"),
          _buildButtonColumn(color, Icons.near_me, "ROUTE"),
          _buildButtonColumn(color, Icons.share, "SHARE"),
        ],
      ),

    );

    Widget titleSection = Container(
      padding: const EdgeInsets.all(32.0),
      child: Row(

        children:[
          Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    padding: const EdgeInsets.only(bottom: 8.0),
                    child: Text(
                      "Google Headquarters",
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                  ),

                  Text(
                    "Mountain View, California, United States",
                    style: TextStyle(color: Colors.grey[505]),
                  ),
                ],
              )
          ),

          Icon(
            Icons.star,
            color: Colors.red[500],
          ),
          Text('41'),
        ],
      ),
    );


    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: "Oreon Inc.",
      home: Scaffold(
        appBar: AppBar(
          title: Text("Googleplex"),
        ),

        body: ListView(
          children: <Widget>[
            Image.asset(
              "images/google.jpeg",
              width: 600,
              height: 241,
              fit: BoxFit.cover,
            ),

            titleSection,

            buttonSection,

            textSection,
          ],
        ),
      ),
    );
  }

  Column _buildButtonColumn(Color color, IconData icon, String label){
    return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,

      children: <Widget>[
        Icon(icon, color: color),

        Container(
          margin: const EdgeInsets.only(top: 8.0),
          child: Text(
            label,
            style: TextStyle(
              fontSize: 12,
              fontWeight: FontWeight.w400,
              color: color,
            ),
          ),
        )

      ],
    );
  }
}

