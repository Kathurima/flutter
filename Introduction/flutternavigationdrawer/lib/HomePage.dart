import 'package:flutter/material.dart';
import 'package:flutternavigationdrawer/main.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      drawer: new NavDrawerCodeOnly(),

       appBar: new AppBar(
         title: new Text("Homepage"),
       ),

      body: new Text("You're in homepage"),
    );
  }
}
