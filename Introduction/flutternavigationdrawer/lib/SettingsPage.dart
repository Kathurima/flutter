import 'package:flutter/material.dart';
import 'package:flutternavigationdrawer/main.dart';

class SettingsPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      drawer: NavDrawerCodeOnly(),

      appBar: new AppBar(
        title: new Text("Settings"),
      ),

      body: new Text("You're in Settings page"),
    );
  }
}
