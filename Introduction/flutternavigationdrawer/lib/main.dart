import 'package:flutter/material.dart';
import 'package:flutternavigationdrawer/HomePage.dart';
import 'package:flutternavigationdrawer/ReviewsPage.dart';
import 'package:flutternavigationdrawer/SettingsPage.dart';

void main() => runApp(
    MyApp()
  );

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: MyDrawerApp(),
    );
  }
}

// Our main file to create our drawer
class MyDrawerApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      drawer: NavDrawerCodeOnly(),

      appBar: new AppBar(
        title: new Text("Drawer"),
      ),

      body: new Text("This is the app's body"),
    );
  }
}

class NavDrawerCodeOnly extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Drawer(
      child: new ListView(
        children: <Widget>[
          new DrawerHeader(
            child: Text("Drawer Header"),
            decoration: new BoxDecoration(
              color: Colors.green,
            ),
          ),

          new ListTile(
            title: new Text("Homepage"),
            onTap: (){
              Navigator.pop(context);
              Navigator.push(context, new MaterialPageRoute(builder: (context) => new HomePage()));
            },
          ),
          new ListTile(
            title: new Text("Settings"),
            onTap: (){
              Navigator.pop(context);
              Navigator.push(context, new MaterialPageRoute(builder: (context) => new SettingsPage()));
            },
          ),
          new ListTile(
            title: new Text("Reviews"),
            onTap: (){
              Navigator.pop(context);
              Navigator.push(context, new MaterialPageRoute(builder: (context) => new ReviewsPage()));
            },
          ),

        ],
      ),
    );
  }
}


