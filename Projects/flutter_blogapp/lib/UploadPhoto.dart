import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutterblogapp/Homepage.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:io';

import 'package:intl/intl.dart';

class UploadImage extends StatefulWidget {
  @override
  _UploadImageState createState() => _UploadImageState();
}

class _UploadImageState extends State<UploadImage> {
  final formKey = new GlobalKey<FormState>();
  String _myValue;

  String url;

  File sampleImage;

  Future getImage() async{
    var tempImage = await ImagePicker.pickImage(source: ImageSource.gallery);

    setState(() {
      sampleImage = tempImage;
    });
  }

  bool validateAndSave(){
    final form = formKey.currentState;

    if(form.validate()){
      form.save();
      return true;

    }else{
      return false;
    }
  }

  //Uploading post to firebase storage and database
  void uploadStatusImage() async{
    if(validateAndSave()){
      final StorageReference postImageRef = FirebaseStorage.instance.ref().child("Post Images");

      // Creating a random key using time to prevent conflicts when two online
      // users upload a status at the same time
      var timeKey = new DateTime.now();

      final StorageUploadTask uploadTask = postImageRef.child(timeKey.toString() + ".jpg").putFile(sampleImage);

      // Getting the url from firebase storage so that its accessible in firebase realtime database
      var imageUrl = await (await uploadTask.onComplete).ref.getDownloadURL();

      url = imageUrl;

      print("Image url is: " + url);

      //Saving image to database
      saveToDatabase(url);

      // Sending user to homepage after uploading image
      goToHomepage();
    }
  }

  void saveToDatabase(url){
    var dbTimeKey = new DateTime.now();
    var formatDate = new DateFormat("MMM, d, yyyy");
    var formatTime = new DateFormat("EEEE, hh:mm aaa");

    String date = formatDate.format(dbTimeKey);
    String time = formatTime.format(dbTimeKey);

    DatabaseReference ref = FirebaseDatabase.instance.reference();

    var data = {
      "image": url,
      "description": _myValue,
      "date": date,
      "time": time,
    };

    ref.child("Posts").push().set(data);

  }

  void goToHomepage(){
    Navigator.push(context, MaterialPageRoute(builder: (context){
      return new Homepage();
    })
    );

  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text("Upload Image"),
        centerTitle: true,
      ),

      body: new Center(
        child: sampleImage == null? Text("Select an image"): enableUpload(),
      ),

      floatingActionButton: new FloatingActionButton(
        onPressed: getImage,
        tooltip: "Add Image",
        child: new Icon(Icons.add_a_photo),
      ),
    );
  }

  // Enabling upload widget
  Widget enableUpload(){
    return Container(
      child: new Form(
          key: formKey,

          child: Column(
            children: <Widget>[
              Image.file(
                sampleImage, height: 330.0, width: 660.0,
              ),

              SizedBox(height: 5.0,),

              TextFormField(
                decoration: new InputDecoration(labelText: "Description"),

                validator: (value){
                  return value.isEmpty ? "Description is required.": null;
                },

                onSaved: (value){
                  return _myValue = value;
                },

              ),

              SizedBox(height: 15.0,),

              RaisedButton(
                elevation: 10.0,
                child: new Text("Add New Post"),
                textColor: Colors.white,
                color: Colors.pink,

                onPressed: uploadStatusImage,
              )

            ],

          )
      ),

    );
  }
}
