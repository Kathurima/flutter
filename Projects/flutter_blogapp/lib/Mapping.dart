import 'package:flutter/material.dart';
import 'LoginRegisterPage.dart';
import 'Homepage.dart';
import 'Authentication.dart';

class MappingPage extends StatefulWidget {
  final AuthImplementation auth;

  MappingPage({
    this.auth,
  });

  @override
  _MappingPageState createState() => _MappingPageState();
}

//enum to check the authentication status of the user
enum AuthStatus{
  signedIn,
  notSignedIn
}

class _MappingPageState extends State<MappingPage> {
  // Initializing auth status
  AuthStatus authStatus = AuthStatus.notSignedIn;

  //initState() - Checking the current status of the user whenever the app is started
  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    widget.auth.getCurrentUser().then((firebaseUserId){
      setState(() {
        authStatus = firebaseUserId == null ? AuthStatus.notSignedIn : AuthStatus.signedIn;
      });
    });

  }

  void _signedIn(){
    setState(() {
      authStatus = AuthStatus.signedIn;
    });
  }

  void _signedOut(){
    setState(() {
      authStatus = AuthStatus.notSignedIn;
    });
  }


  @override
  Widget build(BuildContext context) {

    switch(authStatus){
      case AuthStatus.notSignedIn:
        return new LoginRegisterPage(
          auth: widget.auth,
          onSignedIn: _signedIn
        );

      case AuthStatus.signedIn:
        return new Homepage(
            auth: widget.auth,
            onSignedOut: _signedOut
        );
    }

    return null;
  }
}
