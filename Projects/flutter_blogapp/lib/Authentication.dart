import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

abstract class AuthImplementation{
  Future<String> signIn(String email, String password);
  Future<String> signUp(String email, String password);
  Future<String> getCurrentUser();
  Future<void> signOut();

}

class Auth implements AuthImplementation{
  //Initializing firebase
  final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;

  //Sign In
  Future<String> signIn(String email, String password) async{
    FirebaseUser user = (await _firebaseAuth.signInWithEmailAndPassword(email: email, password: password)).user;
    return user.uid;
  }

  //SignUp
  Future<String> signUp(String email, String password) async{
    FirebaseUser user = (await _firebaseAuth.createUserWithEmailAndPassword(email: email, password: password)).user;
    return user.uid;
  }

  //Getting the ID of the current user
  Future<String> getCurrentUser() async{
    FirebaseUser user = await _firebaseAuth.currentUser();
    return user.uid;
  }

  //Signing out
  Future<void> signOut() async{
    _firebaseAuth.signOut();
  }

}