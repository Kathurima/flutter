import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutterblogapp/DialogBox.dart';
import 'Authentication.dart';

class LoginRegisterPage extends StatefulWidget {

  LoginRegisterPage({
    this.auth,
    this.onSignedIn,
  });

  final AuthImplementation auth;
  final VoidCallback onSignedIn;

  @override
  _LoginRegisterPageState createState() => _LoginRegisterPageState();

}

// enum distinguishes our form types. In this case the login and the register
enum FormType{
  login, register
}

class _LoginRegisterPageState extends State<LoginRegisterPage> {

  DialogBox dialogBox = new DialogBox();

  final formKey = new GlobalKey<FormState>();
  FormType _formType = FormType.login; //Initializing the login form

  String _email = "";
  String _password = "";

  //Methods
  bool validateAndSave(){
    final form = formKey.currentState;

    if(form.validate()){
      form.save();
      return true;

    }else{
      return false;
    }

  }

  void validateAndSubmit() async{
    if(validateAndSave()){
      try{
        if(_formType == FormType.login){
          String userId = await widget.auth.signIn(_email, _password);
          //dialogBox.information(context, "", "Signin successful");
          print("Login user ID = "+ userId);

        }else{
          String userId = await widget.auth.signUp(_email, _password);
          dialogBox.information(context, "Message", "Account created successfully");
          print("Register user ID = "+ userId);
        }

        widget.onSignedIn();

      }catch(e){
        dialogBox.information(context, "Error ", e.toString());
        //print("Error = "+ e);
      }
    }

  }

  void moveToRegister(){
      formKey.currentState.reset(); // Clears all the input data in case the user clicks the already have an account button

      setState(() {
        _formType = FormType.register;
      });
  }

  void moveToLogin(){
    formKey.currentState.reset(); // Clears all the input data in case the user clicks the already have an account button

    setState(() {
      _formType = FormType.login;
    });
  }


  //Design
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text("Oreon Blog"),
      ),

      body: new Container(
        margin: EdgeInsets.all(15.0),

        child: new Form(
          key: formKey,

            child: new Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: createInputs() + createButtons(),
            )

        ),

      ),
    );
  }

  //Input Fields
  List<Widget> createInputs(){
    return [
      SizedBox(height: 10.0),
      logo(),
      SizedBox(height: 20.0),

      new TextFormField(
        decoration: new InputDecoration(labelText: "Email"),

        validator: (value){
          return value.isEmpty ? "Email is required." : null;
        },

        onSaved: (value){
          return _email = value;
        },

      ),

      SizedBox(height: 10.0),

      new TextFormField(
        decoration: new InputDecoration(labelText: "Password"),
        obscureText: true,

        validator: (value){
          return value.isEmpty ? "Password is required." : null;
        },

        onSaved: (value){
          return _password = value;
        },
      ),

      SizedBox(height: 20.0),

    ];
  }

  // Logo
  Widget logo(){
    return new Hero(
        tag: "hero",

        child: new CircleAvatar(
          backgroundColor: Colors.transparent,
          radius: 50.0,
          child: Image.asset("images/oreon_logo.jpg"),
        )
    );
  }

  // Buttons
  List<Widget> createButtons(){
    if(_formType == FormType.login){

      return [
        new RaisedButton(
          child: new Text("Login", style: new TextStyle(fontSize:20.0),),
          textColor: Colors.white,
          color: Colors.pink,

          onPressed: validateAndSubmit,
        ),

        new FlatButton(
          child: new Text("Do not have an account? Create Account?", style: new TextStyle(fontSize:14.0),),
          textColor: Colors.red,
          onPressed: moveToRegister,
        )
      ];

    }else{

      return [
        new RaisedButton(
          child: new Text("Register", style: new TextStyle(fontSize:20.0),),
          textColor: Colors.white,
          color: Colors.pink,

          onPressed: validateAndSubmit,
        ),

        new FlatButton(
          child: new Text("Already have an Account? Go to Login", style: new TextStyle(fontSize:14.0),),
          textColor: Colors.red,
          onPressed: moveToLogin,
        )
      ];
    }

  }
  
}
