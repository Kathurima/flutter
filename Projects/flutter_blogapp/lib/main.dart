import 'package:flutter/material.dart';
import 'package:flutterblogapp/Authentication.dart';
import 'package:flutterblogapp/Mapping.dart';

void main() => runApp(BlogApp());

class BlogApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      debugShowCheckedModeBanner: false,
      title: "Blog App",

      theme: new ThemeData(
        primarySwatch: Colors.pink,
      ),

      home: MappingPage(auth: Auth(),),
    );
  }
}
